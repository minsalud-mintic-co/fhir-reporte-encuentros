# README #

El alcance de la información contenida en este repositorio es facilitar un ejercicio académico de localización y refinamiento de recursos FHIR, para aplicarlos a un escenario de interoperabilidad específico.

No se recomienda el uso en producción de derivaciones de los ejemplos de recursos FHIR asociados a este ejercicio.   

Será necesario un análisis más detallado del escenario de interoperabilidad, prácticas, requisitos, regulaciones, factibilidad y elementos de dato, necesarios para definir un perfil que adapte los recursos FHIR al escenario particular.

## Contenido 

1. Ejemplos de recursos
2. Wiki
	* Definición y alcance
	* Análisis del escenario
	* Descripción de los ejemplos de recursos
	* Guía de prueba de concepto
	* Recomendaciones

## Agradecimientos

* Sr Carlos Jorge Rodriguez Restrepo <cjrodriguez@rc-aci.com>

## Información general

* **Organización:** HL7 Colombia
* **Autor:** Mario Enrique Cortés <mario.cortes@hl7co.org>
* **Revisión:** Dra Nathalia Ortega M. PhD(c) <nathalia.ortega@hl7co.org>
* **Versión:** 0.1.
* **Fecha:** 2019 / Nov / 05
* **Estándar:** HL7 FHIR R4.